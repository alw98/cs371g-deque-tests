// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range
#include <algorithm>
#include <string>
#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test
{
    using deque_type = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using deque_types =
    Types<
        deque<int>,
        my_deque<int>,
        deque<int, allocator<int>>,
        my_deque<int, allocator<int>>>;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, empty0)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, empty1)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, size0)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, size1)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {1, 2, 3, 4, 5, 6};
    ASSERT_EQ(x.size(), 6u);
}

TYPED_TEST(DequeFixture, size2)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(9);
    ASSERT_EQ(x.size(), 9u);
}

TYPED_TEST(DequeFixture, beginend0)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(b, e);
}

TYPED_TEST(DequeFixture, beginend1)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {1};
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(*b, *(--e));
}

TYPED_TEST(DequeFixture, beginend2)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {4, 5, 6, 7, 8, 2, 3, 5, 6, 6};
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(*b, 4);
    ASSERT_EQ(*--e, 6);
}

TYPED_TEST(DequeFixture, push_back0)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    x.push_back(1);
    iterator b = begin(x);
    ASSERT_EQ(*b, 1);
}

TYPED_TEST(DequeFixture, push_back1)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x(3);
    x.push_back(1);
    iterator b = end(x);
    ASSERT_EQ(*--b, 1);
}

TYPED_TEST(DequeFixture, push_back2)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {10, 10, 10, 0, 0, 0};
    x.push_back(0);
    iterator b = end(x);
    ASSERT_EQ(*--b, 0);
}

TYPED_TEST(DequeFixture, pop_back0)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {1};
    x.pop_back();
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(b, e);
}

TYPED_TEST(DequeFixture, pop_back1)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {1};
    x.pop_back();
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(b, e);
}

TYPED_TEST(DequeFixture, pop_back2)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {1, 2, 3};
    x.pop_back();
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(*b, 1);
    ASSERT_EQ(*--e, 2);
}

TYPED_TEST(DequeFixture, resize_one_arg0)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {1, 2, 3};
    x.resize(1);
    ASSERT_EQ(x.size(), 1u);
}

TYPED_TEST(DequeFixture, resize_one_arg1)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {1, 2, 3};
    x.resize(0);
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, resize_one_arg2)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {1, 2, 3};
    x.resize(10);
    deque_type y = {1, 2, 3, 0, 0, 0, 0, 0, 0, 0};
    ASSERT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, resize_two_arg0)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {1, 2, 3};
    x.resize(8, 8);
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(x.size(), 8u);
    ASSERT_EQ(*b, 1);
    ++b;
    ASSERT_EQ(*b, 2);
    ++b;
    ASSERT_EQ(*b, 3);
    ++b;

    while (b != e)
    {
        ASSERT_EQ(*b, 8);
        ++b;
    }
}

TYPED_TEST(DequeFixture, resize_two_arg1)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {1, 2, 3};
    x.resize(2, 3);
    ASSERT_EQ(x.size(), 2u);
    deque_type y = {1, 2};
    ASSERT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, resize_two_arg2)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {1, 2, 3};
    x.resize(10, 10);
    ASSERT_EQ(x.size(), 10u);
    deque_type y = {1, 2, 3, 10, 10, 10, 10, 10, 10, 10};
    ASSERT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, push_front0)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {1, 2, 3};
    x.push_front(8);
    iterator b = begin(x);
    ASSERT_EQ(x.size(), 4u);
    ASSERT_EQ(*b, 8);
    ++b;
    ASSERT_EQ(*b, 1);
    ++b;
    ASSERT_EQ(*b, 2);
    ++b;
    ASSERT_EQ(*b, 3);
}

TYPED_TEST(DequeFixture, push_front1)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    x.push_front(8);
    iterator b = begin(x);
    ASSERT_EQ(x.size(), 1u);
    ASSERT_EQ(*b, 8);
    ++b;
}

TYPED_TEST(DequeFixture, push_front2)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {1, 2, 3};
    x.push_front(8);
    x.push_front(7);
    x.push_front(6);
    ASSERT_EQ(x.size(), 6u);
    deque_type y = {6, 7, 8, 1, 2, 3};
    ASSERT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, pop_front0)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {1, 2, 3};
    x.pop_front();
    iterator b = begin(x);
    ASSERT_EQ(x.size(), 2u);
    ASSERT_EQ(*b, 2);
    ++b;
    ASSERT_EQ(*b, 3);
    ++b;
}

TYPED_TEST(DequeFixture, pop_front1)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x(10, 10);
    x.pop_front();
    x.pop_front();
    x.pop_front();
    iterator b = begin(x);
    ASSERT_EQ(*b, 10);
}

TYPED_TEST(DequeFixture, pop_front2)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 10);
    x.pop_front();
    ASSERT_EQ(x.size(), 99u);
}

TYPED_TEST(DequeFixture, insert0)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {1, 2, 3};
    iterator b = begin(x);
    iterator c = b;
    x.insert(++c, 4);
    ASSERT_EQ(x.size(), 4u);
    ASSERT_EQ(*b, 1);
    ++b;
    ASSERT_EQ(*b, 4);
    ++b;
    ASSERT_EQ(*b, 2);
    ++b;
    ASSERT_EQ(*b, 3);
    ++b;
}

TYPED_TEST(DequeFixture, insert1)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.insert(x.begin(), 4);
    deque_type y = {4};
    ASSERT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, insert2)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {1, 2, 3};
    x.insert(end(x), 4);
    deque_type y = {1, 2, 3, 4};
    ASSERT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, front0)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {1, 2, 3};
    ASSERT_EQ(x.front(), 1);
}

TYPED_TEST(DequeFixture, front1)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 0);
    ASSERT_EQ(x.front(), 0);
}

TYPED_TEST(DequeFixture, front2)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 1);
    ASSERT_EQ(x.front(), 1);
}

TYPED_TEST(DequeFixture, front_const_reference0)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {1, 2, 3};
    ASSERT_EQ(x.front(), 1);
}

TYPED_TEST(DequeFixture, front_const_reference1)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(10, 0);
    ASSERT_EQ(x.front(), 0);
}

TYPED_TEST(DequeFixture, front_const_reference2)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(100, 1);
    ASSERT_EQ(x.front(), 1);
}

TYPED_TEST(DequeFixture, erase0)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {1, 2, 3};
    x.erase(x.begin());
    ASSERT_EQ(*(x.begin()), 2);
}

TYPED_TEST(DequeFixture, erase1)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {1, 2, 3};
    ASSERT_EQ(*(x.erase(x.begin() + 1)), 3);
}

TYPED_TEST(DequeFixture, erase2)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {1, 2, 3};
    ASSERT_EQ(*(x.erase(x.begin())), 2);
}

TYPED_TEST(DequeFixture, end0)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {1, 2, 3};
    ASSERT_EQ(*(--end(x)), 3);
}

TYPED_TEST(DequeFixture, end1)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 1);
    ASSERT_EQ(*(--end(x)), 1);
}

TYPED_TEST(DequeFixture, end2)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(20);
    ASSERT_EQ(*(--end(x)), 0);
}

TYPED_TEST(DequeFixture, end_const_iterator0)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {1, 2, 3};
    ASSERT_EQ(*(--end(x)), 3);
}

TYPED_TEST(DequeFixture, end_const_iterator1)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(100, 1);
    ASSERT_EQ(*(--end(x)), 1);
}

TYPED_TEST(DequeFixture, end_const_iterator2)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(20);
    ASSERT_EQ(*(--end(x)), 0);
}

TYPED_TEST(DequeFixture, clear0)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {1, 2, 3};
    x.clear();
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, clear1)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.clear();
    ASSERT_EQ(x.size(), 0);
}

// TYPED_TEST(DequeFixture, clear2)
// {
//     using deque_type = typename TestFixture::deque_type;
//     deque_type x(100, 5);
//     x.clear();
//     ASSERT_EQ(x.size(), 0);
// }

TYPED_TEST(DequeFixture, begin0)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {1, 2, 3};
    ASSERT_EQ(*(x.begin()), 1);
}

TYPED_TEST(DequeFixture, begin1)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 1);
    ASSERT_EQ(*(x.begin()), 1);
}

TYPED_TEST(DequeFixture, begin2)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100);
    ASSERT_EQ(*(x.begin()), 0);
}

TYPED_TEST(DequeFixture, begin_const0)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {1, 2, 3};
    ASSERT_EQ(*(x.begin()), 1);
}

TYPED_TEST(DequeFixture, begin_const1)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(100, 1);
    ASSERT_EQ(*(x.begin()), 1);
}

TYPED_TEST(DequeFixture, begin_const2)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(100);
    ASSERT_EQ(*(x.begin()), 0);
}

TYPED_TEST(DequeFixture, back0)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {1, 2, 3};
    ASSERT_EQ(x.back(), 3);
    x.back() = 4;
    ASSERT_EQ(x.back(), 4);
}

TYPED_TEST(DequeFixture, back1)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 20);
    ASSERT_EQ(x.back(), 20);
    x.back() = 4;
    ASSERT_EQ(x.back(), 4);
}

TYPED_TEST(DequeFixture, back2)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(50);
    ASSERT_EQ(x.back(), 0);
    x.back() = 4;
    ASSERT_EQ(x.back(), 4);
}

TYPED_TEST(DequeFixture, back_const0)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {1, 2, 3};
    ASSERT_EQ(x.back(), 3);
}

TYPED_TEST(DequeFixture, back_const1)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(100, 20);
    ASSERT_EQ(x.back(), 20);
}

TYPED_TEST(DequeFixture, back_const2)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(50);
    ASSERT_EQ(x.back(), 0);
}

TYPED_TEST(DequeFixture, at0)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {1, 2, 3};
    ASSERT_EQ(x.at(1), 2);
    x.at(1) = 10;
    ASSERT_EQ(x.at(1), 10);
}

TYPED_TEST(DequeFixture, at1)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 100);
    ASSERT_EQ(x.at(x.size() - 1), 100);
}

TYPED_TEST(DequeFixture, at2)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100);
    ASSERT_EQ(x.at(0), 0);
}

TYPED_TEST(DequeFixture, at_const0)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {1, 2, 3};
    ASSERT_EQ(x.at(1), 2);
}

TYPED_TEST(DequeFixture, at_const1)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(100, 100);
    ASSERT_EQ(x.at(x.size() - 1), 100);
}

TYPED_TEST(DequeFixture, at_const2)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(100);
    ASSERT_EQ(x.at(0), 0);
}

TYPED_TEST(DequeFixture, operator_bracket0)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {1, 2, 3};
    ASSERT_EQ(x[1], 2);
    x[1] = 10;
    ASSERT_EQ(x[1], 10);
}

TYPED_TEST(DequeFixture, operator_bracket1)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 100);
    ASSERT_EQ(x[x.size() - 1], 100);
}

TYPED_TEST(DequeFixture, operator_bracket2)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100);
    ASSERT_EQ(x[0], 0);
}

TYPED_TEST(DequeFixture, operator_bracket_const0)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {1, 2, 3};
    ASSERT_EQ(x[1], 2);
}

TYPED_TEST(DequeFixture, operator_bracket_const1)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {1, 2, 3};
    ASSERT_EQ(x[1], 2);
}

TYPED_TEST(DequeFixture, operator_bracket_const2)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(100);
    ASSERT_EQ(x[0], 0);
}

TYPED_TEST(DequeFixture, operator_equal0)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {1, 2, 3};
    const deque_type y = x;
    ASSERT_EQ(y[0], 1);
    ASSERT_EQ(y[1], 2);
    ASSERT_EQ(y[2], 3);
}

TYPED_TEST(DequeFixture, operator_equal1)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(20, 5);
    const deque_type y = x;
    ASSERT_EQ(y[0], 5);
    ASSERT_EQ(y[1], 5);
    ASSERT_EQ(y[2], 5);
}

TYPED_TEST(DequeFixture, operator_equal2)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(20);
    const deque_type y = x;
    ASSERT_EQ(y[0], 0);
    ASSERT_EQ(y[1], 0);
    ASSERT_EQ(y[2], 0);
}

TYPED_TEST(DequeFixture, constructor_copy0)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {1, 2, 3};
    const deque_type y(x);
    ASSERT_EQ(y[0], 1);
    ASSERT_EQ(y[1], 2);
    ASSERT_EQ(y[2], 3);
}

TYPED_TEST(DequeFixture, constructor_copy1)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(100, 100);
    const deque_type y(x);
    ASSERT_EQ(y[99], 100);
    ASSERT_EQ(y[98], 100);
    ASSERT_EQ(y[97], 100);
}

TYPED_TEST(DequeFixture, constructor_copy2)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type z;
    const deque_type x(100);
    const deque_type y(x);
    ASSERT_EQ(y[0], 0);
    ASSERT_EQ(y[1], 0);
    ASSERT_EQ(y[2], 0);
}

TYPED_TEST(DequeFixture, constructor_initializer_list_allocator0)
{
    using deque_type = typename TestFixture::deque_type;
    std::initializer_list<int> a = {1, 2, 3};
    const allocator<int> b;
    deque_type y(a, b);
    ASSERT_EQ(y[0], 1);
    ASSERT_EQ(y[1], 2);
    ASSERT_EQ(y[2], 3);
}

TYPED_TEST(DequeFixture, constructor_initializer_list_allocator1)
{
    using deque_type = typename TestFixture::deque_type;
    std::initializer_list<int> a;
    const allocator<int> b;
    deque_type y(a, b);
    ASSERT_EQ(y.size(), 0u);
}

TYPED_TEST(DequeFixture, constructor_initializer_list_allocator2)
{
    using deque_type = typename TestFixture::deque_type;
    std::initializer_list<int> a = {1, 2, 3, 4, 5};
    const allocator<int> b;
    deque_type y(a, b);
    deque_type z = {1, 2, 3, 4, 5};
    ASSERT_TRUE(y == z);
}

TYPED_TEST(DequeFixture, constructor_initializer_list0)
{
    using deque_type = typename TestFixture::deque_type;
    std::initializer_list<int> a = {1, 2, 3};
    deque_type y(a);
    ASSERT_EQ(y[0], 1);
    ASSERT_EQ(y[1], 2);
    ASSERT_EQ(y[2], 3);
}

TYPED_TEST(DequeFixture, constructor_initializer_list1)
{
    using deque_type = typename TestFixture::deque_type;
    std::initializer_list<int> a;
    deque_type y(a);
    ASSERT_EQ(y.size(), 0u);
}

TYPED_TEST(DequeFixture, constructor_initializer_list2)
{
    using deque_type = typename TestFixture::deque_type;
    std::initializer_list<int> a = {1, 2, 3, 4, 5};
    deque_type y(a);
    deque_type z = {1, 2, 3, 4, 5};
    ASSERT_TRUE(y == z);
}

TYPED_TEST(DequeFixture, constructor_size_const_allocator0)
{
    using deque_type = typename TestFixture::deque_type;
    const allocator<int> b;
    deque_type y(3, 2, b);
    ASSERT_EQ(y[0], 2);
    ASSERT_EQ(y[1], 2);
    ASSERT_EQ(y[2], 2);
}

TYPED_TEST(DequeFixture, constructor_size_const_allocator1)
{
    using deque_type = typename TestFixture::deque_type;
    const allocator<int> b;
    deque_type y(100, 2, b);
    ASSERT_EQ(y[99], 2);
    ASSERT_EQ(y[98], 2);
    ASSERT_EQ(y[97], 2);
}

TYPED_TEST(DequeFixture, constructor_size_const_allocator2)
{
    using deque_type = typename TestFixture::deque_type;
    const allocator<int> b;
    deque_type y(0, 2, b);
    ASSERT_EQ(y.size(), 0);
}

TYPED_TEST(DequeFixture, constructor_size_const0)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type y(3, 4);
    ASSERT_EQ(y[0], 4);
    ASSERT_EQ(y[1], 4);
    ASSERT_EQ(y[2], 4);
}

TYPED_TEST(DequeFixture, constructor_size_const1)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type y(0, 7);
    ASSERT_EQ(y.size(), 0);
}

TYPED_TEST(DequeFixture, constructor_size_const2)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type y(100, 10);
    ASSERT_EQ(y[99], 10);
    ASSERT_EQ(y[98], 10);
    ASSERT_EQ(y[97], 10);
}

TYPED_TEST(DequeFixture, constructor_explicit0)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type y(100);
    ASSERT_EQ(y[0], 0);
    ASSERT_EQ(y[1], 0);
    ASSERT_EQ(y[2], 0);
    ASSERT_EQ(y.size(), 100);
}

TYPED_TEST(DequeFixture, constructor_explicit1)
{
    using deque_type = typename TestFixture::deque_type;
    const allocator<char> b;
    deque_type y(0);
    ASSERT_EQ(y.size(), 0);
}

TYPED_TEST(DequeFixture, constructor_explicit2)
{
    using deque_type = typename TestFixture::deque_type;
    const allocator<char> b;
    deque_type y(20);
    ASSERT_EQ(y[19], 0);
    ASSERT_EQ(y[18], 0);
    ASSERT_EQ(y[17], 0);
    ASSERT_EQ(y.size(), 20);
}

TYPED_TEST(DequeFixture, double_equal0)
{
    using deque_type = typename TestFixture::deque_type;
    const allocator<char> b;
    deque_type x = {1, 2, 3};
    deque_type y = {1, 2, 3};
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, double_equal1)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3});
    deque_type y = {1, 2, 3};
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, less_than0)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3});
    deque_type y = {1, 2, 3, 4};
    ASSERT_LT(x, y);
}

TYPED_TEST(DequeFixture, less_than1)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3});
    deque_type y = {4, 2, 3};
    ASSERT_LT(x, y);
}

TYPED_TEST(DequeFixture, less_than2)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3});
    deque_type y = {4, 2, 3, 1};
    ASSERT_LT(x, y);
}

TYPED_TEST(DequeFixture, swap0)
{
    using deque_type = typename TestFixture::deque_type;
    const allocator<char> b;
    deque_type x = {1, 2};
    deque_type y = {1, 2, 3};
    swap(x, y);
    ASSERT_EQ(y[0], 1);
    ASSERT_EQ(y[1], 2);
    ASSERT_EQ(x[0], 1);
    ASSERT_EQ(x[1], 2);
    ASSERT_EQ(x[2], 3);
}

TYPED_TEST(DequeFixture, swap1)
{
    using deque_type = typename TestFixture::deque_type;
    const allocator<char> b;
    deque_type x;
    deque_type y = {1, 2, 3};
    deque_type z = {1, 2, 3};
    swap(x, y);
    ASSERT_TRUE(x == z);
}

TYPED_TEST(DequeFixture, swap2)
{
    using deque_type = typename TestFixture::deque_type;
    const allocator<char> b;
    deque_type x(100, 10);
    deque_type y = {1, 2, 3};
    swap(x, y);
    ASSERT_EQ(y.size(), 100);
}

TYPED_TEST(DequeFixture, mixed0)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type y({1, 2, 3});
    auto b = begin(y);
    ++b;
    EXPECT_TRUE(y[1] == 2);
    deque_type x(10, 10);
    for (int i = 1; i <= 5; ++i)
        x.push_back(i);
    EXPECT_TRUE(x[x.size() - 1] == 5);
    EXPECT_TRUE(x[x.size() - 2] == 4);
    EXPECT_TRUE(x[x.size() - 3] == 3);
    EXPECT_TRUE(x[x.size() - 4] == 2);
    EXPECT_TRUE(x[x.size() - 5] == 1);

    // 10 10 10 10 10 10 10 10 10 10 1 2 3 4 5

    for (int i = 0; i < 5; ++i)
    {
        x.push_front(i);
    }

    EXPECT_TRUE(x[0] == 4);
    EXPECT_TRUE(x[1] == 3);
    EXPECT_TRUE(x[2] == 2);
    EXPECT_TRUE(x[3] == 1);
    EXPECT_TRUE(x[4] == 0);
    EXPECT_TRUE(x[5] == 10);
}

TYPED_TEST(DequeFixture, mixed1)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(1);
    for (int i = 0; i < 5; ++i)
    {
        x.push_back(10);
    }
    deque_type y = {0, 10, 10, 10, 10, 10};
    ASSERT_EQ(x, y);
}