// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;};

using
    deque_types =
    Types<
           deque<int>,
        my_deque<int>,
           deque<int, allocator<int>>,
        my_deque<int, allocator<int>>>;

#ifdef __APPLE__
    TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
    TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);}

TYPED_TEST(DequeFixture, test2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test3) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, testPushBack) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    ASSERT_EQ(x.size(), 0);
    x.push_back(1);
    ASSERT_EQ(x[0], 1);
    ASSERT_EQ(x.size(), 1);
    x.push_back(2);
    ASSERT_EQ(x[1], 2);
    ASSERT_EQ(x.size(), 2);
}

TYPED_TEST(DequeFixture, testPushBackMany) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    const int count = 11;
    for(int i = 0; i < count; ++i)
        x.push_back(i);
    ASSERT_EQ(x.size(), count);
    for(int i = 0; i < count; ++i)
        ASSERT_EQ(x[i], i);
}

TYPED_TEST(DequeFixture, testPushBackTillResize) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    const int count = 100;
    for(int i = 0; i < count; ++i)
        x.push_back(i);
    ASSERT_EQ(x.size(), count);
    for(int i = 0; i < count; ++i)
        ASSERT_EQ(x[i], i);
}

TYPED_TEST(DequeFixture, testPushBackMaintainRef) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;    
    const int count = 100;
    int* refs[count];
    for(int i = 0; i < count; ++i){
        x.push_back(i);
        refs[i] = &x[i];
    }
    ASSERT_EQ(x.size(), count);
    for(int i = 0; i < count; ++i)
        ASSERT_EQ(&x[i], refs[i]);
}

TYPED_TEST(DequeFixture, testPushBackManyMaintainRef) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    const int count = 10000;
    int* refs[count];
    for(int i = 0; i < count; ++i){
        x.push_back(i);
        refs[i] = &x[i];
    }
    ASSERT_EQ(x.size(), count);
    for(int i = 0; i < count; ++i)
        ASSERT_EQ(&x[i], refs[i]);
}

TYPED_TEST(DequeFixture, testPushFront) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    ASSERT_EQ(x.size(), 0);
    x.push_front(1);
    ASSERT_EQ(x[0], 1);
    ASSERT_EQ(x.size(), 1);
    x.push_front(2);
    ASSERT_EQ(x[0], 2);
    ASSERT_EQ(x.size(), 2);
}

TYPED_TEST(DequeFixture, testPushFrontMany) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    const int count = 25;
    for(int i = 0; i < count; ++i)
        x.push_front(i);
    ASSERT_EQ(x.size(), count);
    for(int i = 0; i < count; ++i)
        ASSERT_EQ(x[i], count - i - 1);
}

TYPED_TEST(DequeFixture, testPushFrontTillResize) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    const int count = 100;
    for(int i = 0; i < count; ++i)
        x.push_front(i);
    ASSERT_EQ(x.size(), count);
    for(int i = 0; i < count; ++i)
        ASSERT_EQ(x[i], count - i - 1);
}

TYPED_TEST(DequeFixture, testPushFrontMaintainRef) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;    
    const int count = 100;
    int* refs[count];
    for(int i = 0; i < count; ++i){
        x.push_front(i);
        refs[i] = &x[0];
    }
    ASSERT_EQ(x.size(), count);
    for(int i = 0; i < count; ++i)
        ASSERT_EQ(&x[i], refs[count - i - 1]);
}

TYPED_TEST(DequeFixture, testPushFrontManyMaintainRef) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    const int count = 10000;
    int* refs[count];
    for(int i = 0; i < count; ++i){
        x.push_front(i);
        refs[i] = &x[0];
    }
    ASSERT_EQ(x.size(), count);
    for(int i = 0; i < count; ++i)
        ASSERT_EQ(&x[i], refs[count - i - 1]);
}


TYPED_TEST(DequeFixture, testPopFront1){
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(1);
    ASSERT_EQ(x[0], 1);
    x.push_back(2);
    ASSERT_EQ(x[0], 1);
    x.pop_front();
    ASSERT_EQ(x[0], 2);
    ASSERT_EQ(x.size(), 1);
}

TYPED_TEST(DequeFixture, testPopFront2){
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(1);
    x.pop_front();
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, testPopFront3){
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(1);
    x.push_back(2);
    x.push_back(3);
    ASSERT_EQ(x.size(), 3);
    x.pop_front();
    ASSERT_EQ(x.size(), 2);
    x.pop_front();
    ASSERT_EQ(x.size(), 1);
    x.pop_front();
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, testPopFront4){
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    const int count = 100;

    for(int i = 0; i < count; ++i)
        x.push_back(i);
    for(int i = 0; i < count; ++i){
        ASSERT_EQ(x[0], i);
        x.pop_front();
    }
}

TYPED_TEST(DequeFixture, testPopFront5){
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    const int count = 100;

    for(int i = 0; i < count; ++i){
        x.push_back(i);
        ASSERT_EQ(x[0], i);
        x.pop_front();
        ASSERT_EQ(x.size(), 0);
    }
    for(int i = 0; i < count * 2; ++i){
        x.push_front(i);
        ASSERT_EQ(x[0], i);
        x.pop_front();
        ASSERT_EQ(x.size(), 0);
    }
}

TYPED_TEST(DequeFixture, testPopBack1){
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(1);
    x.pop_back();
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, testPopBack2){
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(1);
    x.pop_back();
    x.push_back(2);
    ASSERT_EQ(x[0], 2);
    x.pop_back();
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, testPopBack3){
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    
    const int count = 100;
    for(int i = 0; i < count; ++i){
        x.push_front(i);
    }

    for(int i = 0; i < count; ++i){
        ASSERT_EQ(x[x.size() - 1], i);
        x.pop_back();
    }
}

TYPED_TEST(DequeFixture, testIndexing){
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(1);
    ASSERT_EQ(x[0], 1);
    x[0] = 2;
    ASSERT_EQ(x[0], 2);
    x.push_back(x[0]);
    ASSERT_EQ(x[1], 2);
    x[1] = 0;
    ASSERT_EQ(x[1], 0);
}



TYPED_TEST(DequeFixture, testAt1){
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    try{
        int a = x.at(0);
        ASSERT_EQ(a, ~a);

    } catch(exception& e){
    }
}

TYPED_TEST(DequeFixture, testAt2){
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(1);
    ASSERT_EQ(x.at(0), 1);
    x.at(0) = 2;
    ASSERT_EQ(x.at(0), 2);
}

TYPED_TEST(DequeFixture, testFront){
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(1);
    x.push_back(2);
    ASSERT_EQ(x.front(), 1);
    x.front() = 4;
    ASSERT_EQ(x[0], 4);
    x.pop_front();
    ASSERT_EQ(x.front(), 2);
}

TYPED_TEST(DequeFixture, testBack){
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(1);
    x.push_back(2);
    ASSERT_EQ(x.back(), 2);
    x.back() = 4;
    ASSERT_EQ(x[1], 4);
    x.pop_back();
    ASSERT_EQ(x.back(), 1);
}

TYPED_TEST(DequeFixture, testIt1){
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_back(0);
    x.push_back(1);
    x.push_back(2);
    x.push_back(3);
    x.push_back(4);

    iterator it = begin(x);
    iterator en = end(x);

    int i = 0;
    while(it != en){
        ASSERT_EQ(*it, i);
        ++it;
        ++i;
    }

    ASSERT_EQ(i, 5);
}

TYPED_TEST(DequeFixture, testIt2){
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_back(0);
    x.push_back(0);
    x.push_back(0);
    x.push_back(0);
    x.push_back(0);

    iterator it = begin(x);
    iterator en = end(x);

    int i = 0;
    while(it != en){
        *it = i;
        ++it;   
        ++i;
    }

    ASSERT_EQ(x[0], 0);
    ASSERT_EQ(x[1], 1);
    ASSERT_EQ(x[2], 2);
    ASSERT_EQ(x[3], 3);
    ASSERT_EQ(x[4], 4);
}

TYPED_TEST(DequeFixture, testIt3){
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_back(0);
    x.push_back(1);
    x.push_back(2);
    x.push_back(3);
    x.push_back(4);

    iterator it = begin(x);
    
    it += 1;
    *it = 0;
    it+=2;
    *it = 8;

    ASSERT_EQ(x[0], 0);
    ASSERT_EQ(x[1], 0);
    ASSERT_EQ(x[2], 2);
    ASSERT_EQ(x[3], 8);
    ASSERT_EQ(x[4], 4);
}

TYPED_TEST(DequeFixture, testConstIt){
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    deque_type x;
    x.push_back(0);
    x.push_back(1);
    x.push_back(2);
    x.push_back(3);
    x.push_back(4);

    const deque_type& y = x;
    const_iterator it = begin(y);
    ASSERT_EQ(*it, 0);
    ++it;
    ASSERT_EQ(*it, 1);
    it += 2;
    ASSERT_EQ(*it, 3);
}

TYPED_TEST(DequeFixture, resize1){
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;

    x.resize(5);
    ASSERT_EQ(x.size(), 5);

    iterator b = begin(x);
    iterator e = end(x);
    while(b != e){
        ASSERT_EQ(*b, 0);
        ++b;
    }

    x.resize(1);
    ASSERT_EQ(x[0], 0);

    x.resize(10, 2);
    iterator b2 = begin(x);
    ASSERT_EQ(*b2, 0);
    iterator e2 = end(x);
    ++b2;

    while(b2 != e2){
        ASSERT_EQ(*b2, 2);
        ++b2;
    }

    x.resize(0);
    ASSERT_EQ(x.size(), 0);

    x.resize(500, 99);
    ASSERT_EQ(x.size(), 500);

    iterator b3 = begin(x);
    iterator e3 = end(x);

    while(b3 != e3){
        ASSERT_EQ(*b3, 99);
        ++b3;
    }
}


TYPED_TEST(DequeFixture, construct1){
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x(5);
    ASSERT_EQ(x.size(), 5);
    iterator b = begin(x);
    iterator e = end(x);
    while(b != e){
        ASSERT_EQ(*b, 0);
        ++b;
    }
}

TYPED_TEST(DequeFixture, construct2){
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x(5, 5);
    ASSERT_EQ(x.size(), 5);
    iterator b = begin(x);
    iterator e = end(x);
    while(b != e){
        ASSERT_EQ(*b, 5);
        ++b;
    }
}

TYPED_TEST(DequeFixture, construct3){
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4, 5});
    ASSERT_EQ(x.size(), 5);
    for(int i = 0; i < 5; ++i){
        ASSERT_EQ(x[i], i + 1);
    }
}

TYPED_TEST(DequeFixture, construct4){
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4, 5});
    deque_type y(x);
    
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, erase1){
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;

    deque_type x({1, 2, 3, 4, 5});
    iterator b = begin(x);

    x.erase(b);

    ASSERT_EQ(x, deque_type({2, 3, 4, 5}));

    iterator e = end(x) - 1;
    x.erase(e);
    ASSERT_EQ(x, deque_type({2, 3, 4}));
    ASSERT_TRUE(e == end(x));
}