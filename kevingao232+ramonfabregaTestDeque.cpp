// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test
{
    using deque_type = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using deque_types =
    Types<
        deque<int>,
        my_deque<int>,
        deque<int, allocator<int>>,
        my_deque<int, allocator<int>>>;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

// empty
TYPED_TEST(DequeFixture, test1)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

// iterator
TYPED_TEST(DequeFixture, test2)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(b, e);
}

// const iterator
TYPED_TEST(DequeFixture, test3)
{
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(b, e);
}

// constructor (size_type s, const_reference v)
TYPED_TEST(DequeFixture, test_4)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(18, 5);
    deque_type y(18, 5);
    ASSERT_EQ(x, y);
}

// constructor (size_type s, const_reference v)
TYPED_TEST(DequeFixture, test_5)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(25, 9);
    deque_type y(25, 9);
    ASSERT_EQ(x, y);
}

// constructor (size_type s)
TYPED_TEST(DequeFixture, test_6)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(9);
    deque_type y(9);
    ASSERT_EQ(x, y);
}

// copy constructor (size_type s)
TYPED_TEST(DequeFixture, test_7)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(9);
    deque_type y(x);
    ASSERT_EQ(x, y);
}

// copy constructor (size_type s, const_reference v)
TYPED_TEST(DequeFixture, test_8)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(9, 2);
    deque_type y(x);
    ASSERT_EQ(x, y);
}

// length 0 deque => b == e
TYPED_TEST(DequeFixture, test_9)
{
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x(0);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == e);
}

// length 1 deque => b != e
TYPED_TEST(DequeFixture, test_10)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x(1);
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b != e);
}

// non equality
TYPED_TEST(DequeFixture, test_11)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x(10, 4);
    deque_type y(10, 5);
    EXPECT_TRUE(x != y);
}

// < operator
TYPED_TEST(DequeFixture, test_12)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x(10, 4);
    deque_type y(10, 5);
    EXPECT_TRUE(x < y);
}

TYPED_TEST(DequeFixture, test_13)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x(10, 4);
    auto b = begin(x);
    auto e = end(x);
    EXPECT_TRUE(b != e);
}

// += iterator
TYPED_TEST(DequeFixture, test_14)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x(3);
    auto b = begin(x);
    auto e = end(x);
    b += 3;
    EXPECT_TRUE(b == e);
}

// ++ (preincr) iterator
TYPED_TEST(DequeFixture, test_15)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x(1);
    auto b = begin(x);
    auto e = end(x);
    ++b;
    EXPECT_TRUE(b == e);
}

// ++ (postincr) iterator
TYPED_TEST(DequeFixture, test_16)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x(1);
    auto b = begin(x);
    auto e = end(x);
    b++;
    EXPECT_TRUE(b == e);
}

// -= iterator
TYPED_TEST(DequeFixture, test_17)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x(3);
    auto b = begin(x);
    auto e = end(x);
    e -= 3;
    EXPECT_TRUE(b == e);
}

// -- (preincr) iterator
TYPED_TEST(DequeFixture, test_18)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x(1);
    auto b = begin(x);
    auto e = end(x);
    --e;
    EXPECT_TRUE(b == e);
}

// ++ (postincr) iterator
TYPED_TEST(DequeFixture, test_19)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x(1);
    auto b = begin(x);
    auto e = end(x);
    e--;
    EXPECT_TRUE(b == e);
}

// -- && ++
TYPED_TEST(DequeFixture, test_20)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x(2);
    auto b = begin(x);
    auto e = end(x);
    ++b;
    --e;
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test_21)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x(1, 9);
    auto b = begin(x);
    EXPECT_TRUE(*b == 9);
}

// pre-init value
TYPED_TEST(DequeFixture, test_22)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x(1);
    auto b = begin(x);
    EXPECT_TRUE(*b == 0);
}

TYPED_TEST(DequeFixture, test_23)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x(1, 9);
    auto b = begin(x);
    auto e = end(x);
    ++b;
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test_24)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x(1, 9);
    auto b = begin(x);
    auto e = end(x);
    --e;
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test_25)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x(3, 9);
    auto b = begin(x);
    auto e = end(x);
    b += 3;
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test_26)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x(3, 9);
    auto b = begin(x);
    auto e = end(x);
    e -= 3;
    EXPECT_TRUE(b == e);
}

// push back
TYPED_TEST(DequeFixture, test_27)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x({1, 2, 3, 4, 5});
    int num = 6;
    x.push_back(num);
    deque_type y({1, 2, 3, 4, 5, 6});
    EXPECT_TRUE(x == y);
}

// push front
TYPED_TEST(DequeFixture, test_28)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x({1, 2, 3, 4, 5});
    int num = 6;
    x.push_front(num);
    deque_type y({6, 1, 2, 3, 4, 5});
    EXPECT_TRUE(x == y);
}

// resize to 5 length, fill with 4's
TYPED_TEST(DequeFixture, test_29)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x({1, 2});
    int n = 4;
    x.resize(5, n);
    deque_type y({1, 2, 4, 4, 4});
    EXPECT_TRUE(x == y);
}

// swap -> 2 args
TYPED_TEST(DequeFixture, test_30)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x1({1, 2, 3});
    deque_type y1({4, 5, 6, 7, 8});
    x1.swap(y1);
    deque_type x2({4, 5, 6, 7, 8});
    deque_type y2({1, 2, 3});
    EXPECT_TRUE(x1 == x2);
    EXPECT_TRUE(y1 == y2);
    EXPECT_TRUE(x1.size() == 5);
    EXPECT_TRUE(y1.size() == 3);
}

// swap -> 1 arg
TYPED_TEST(DequeFixture, test_31)
{
    using deque_type = typename TestFixture::deque_type;

    deque_type x(0);
    x.resize(4);
    deque_type y({0, 0, 0, 0});
    EXPECT_TRUE(x == y);
}
