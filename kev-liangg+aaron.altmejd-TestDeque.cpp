// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using
deque_types =
    Types<
//           deque<int>,
    my_deque<int>,
//           deque<int, allocator<int>>,
    my_deque<int, allocator<int>>>;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, empty0) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, empty1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;

    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, empty2) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;

    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, empty3) {
    using deque_type     = typename TestFixture::deque_type;

    deque_type x;
    ASSERT_TRUE(x.empty());
    x.push_back(1);
    ASSERT_TRUE(!x.empty());
    x.pop_back();
    ASSERT_TRUE(x.empty());
}

TYPED_TEST(DequeFixture, construct0) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x(5);
    deque<int> y(5);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, construct1) {
    using deque_type     = typename TestFixture::deque_type;

    const deque_type x(5);
    const deque<int> y(5);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, construct2) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x(6, 3);
    deque<int> y(6, 3);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, construct3) {
    using deque_type = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;

    allocator_type a;
    deque_type x(6, 3, a);
    deque<int> y(6, 3, a);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, construct4) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20});
    deque<int> y({1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20});
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, construct5) {
    using deque_type = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;
    allocator_type a;

    deque_type x({1,2,3,4,5}, a);
    deque<int> y({1,2,3,4,5}, a);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, construct6) {
    using deque_type = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;
    allocator_type a;

    deque_type x({1,2,3,4,5}, a);
    deque<int> y({1,2,3,4,5}, a);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, construct7) {
    using deque_type = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;
    allocator_type a;

    deque_type x_init({1,2,3,4,5}, a);
    deque_type x(x_init);
    deque<int> y_init({1,2,3,4,5}, a);
    deque<int> y(y_init);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, construct8) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({1,2,3,4,5,6,7,8,9,10});
    deque_type y = x;
    deque<int> a({1,2,3,4,5,6,7,8,9,10});
    deque<int> b = a;
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
    ASSERT_TRUE(equal(y.begin(), y.end(), b.begin(), b.end()));
}

TYPED_TEST(DequeFixture, access0) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x(200);
    for (int i = 0; i < 200; i++) {
        ASSERT_EQ (0, x[i]);
    }
}

TYPED_TEST(DequeFixture, access1) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({0,1,2,3,4,5});
    for (int i = 0; i < 6; i++) {
        ASSERT_EQ (i, x.at(i));
    }
}

TYPED_TEST(DequeFixture, access2) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({0,1,2,3,4,5});
    for (int i = 0; i < 6; i++) {
        ASSERT_EQ (i, x[i]);
    }
}

TYPED_TEST(DequeFixture, access3) {
    using deque_type = typename TestFixture::deque_type;

    const deque_type x({0,1,2,3,4,5});
    for (int i = 0; i < 6; i++) {
        ASSERT_EQ (i, x.at(i));
    }
}

TYPED_TEST(DequeFixture, access4) {
    using deque_type = typename TestFixture::deque_type;

    const deque_type x({0,1,2,3,4,5});
    for (int i = 0; i < 6; i++) {
        ASSERT_EQ (i, x[i]);
    }
}

TYPED_TEST(DequeFixture, access5) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({1,2,3,4,5});
    try {
        x.at(-1);
        ASSERT_TRUE(false);
    }
    catch (std::out_of_range &e) {
        ASSERT_TRUE(true);
    }

    try {
        x.at(5);
        ASSERT_TRUE(false);
    }
    catch (std::out_of_range &e) {
        ASSERT_TRUE(true);
    }
}

TYPED_TEST(DequeFixture, shrink0) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({1,3,5,7,9,11});
    deque<int> y({1,3,5,7,9,11});
    x.resize(1);
    y.resize(1);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, shrink1) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20});
    deque<int> y({1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20});
    x.resize(5);
    y.resize(5);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, swap0) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x1({1, 2, 3, 4, 5, 6, 7});
    deque_type x2({7, 6, 5, 4, 3, 2, 1});
    deque<int> y1({1, 2, 3, 4, 5, 6, 7});
    deque<int> y2({7, 6, 5, 4, 3, 2, 1});
    swap(x1, x2);
    swap(y1, y2);
    ASSERT_TRUE(equal(x1.begin(), x1.end(), y1.begin(), y1.end()));
    ASSERT_TRUE(equal(x2.begin(), x2.end(), y2.begin(), y2.end()));
}

TYPED_TEST(DequeFixture, swap1) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x1({1, 2, 3, 4, 5, 6, 7});
    deque_type x2({7, 6, 5, 4, 3, 2, 1});
    deque<int> y1({1, 2, 3, 4, 5, 6, 7});
    deque<int> y2({7, 6, 5, 4, 3, 2, 1});
    x1.swap(x2);
    y1.swap(y2);
    ASSERT_TRUE(equal(x1.begin(), x1.end(), y1.begin(), y1.end()));
    ASSERT_TRUE(equal(x2.begin(), x2.end(), y2.begin(), y2.end()));
}


TYPED_TEST(DequeFixture, grow0) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({1,2,3,4,5});
    deque<int> y({1,2,3,4,5});
    x.resize(20);
    y.resize(20);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, grow1) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({1,2,3,4,5});
    deque<int> y({1,2,3,4,5});
    x.resize(10);
    y.resize(10);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}


TYPED_TEST(DequeFixture, push_front0) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({});
    deque<int> y({});
    for (int i=0; i<64; i++) {
        x.push_front(i);
        y.push_front(i);
    }
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, push_front1) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({});
    deque<int> y({});
    for (int i=0; i<64; i++) {
        x.push_front(i);
        y.push_front(i);
    }
    for (int i=0; i<63; i++) {
        ASSERT_EQ(x.at(i), 63-i);
    }
}

TYPED_TEST(DequeFixture, push_back0) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({});
    deque<int> y({});
    for (int i=0; i<64; i++) {
        x.push_back(i);
        y.push_back(i);
    }
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, push_back1) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({});
    deque<int> y({});
    for (int i=0; i<64; i++) {
        x.push_back(i);
        y.push_back(i);
    }
    for (int i=0; i<63; i++) {
        ASSERT_EQ(x.at(i), i);
    }
}

TYPED_TEST(DequeFixture, push_symmetric0) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x;
    deque<int> y;
    for (int i=0; i<64; i++) {
        // x.push_front(i);
        // y.push_front(i);
        x.push_back(i);
        y.push_back(i);
    }
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, erase0) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({5, 4, 3, 2, 1});
    deque<int> y({5, 4, 3, 2, 1});
    x.erase(x.begin() + 1);
    y.erase(y.begin() + 1);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
    x.erase(x.begin() + 1);
    y.erase(y.begin() + 1);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, erase1) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({});
    deque<int> y({});
    for (int i=0; i<64; i++) {
        x.push_back(i);
        y.push_back(i);
    }
    for (int i=0; i<32; i++) {
        x.erase(x.begin()+i);
        y.erase(y.begin()+i);
    }
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, pop_back0) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({});
    deque<int> y({});
    for (int i=0; i<64; i++) {
        x.push_back(i);
        y.push_back(i);
    }
    for (int i=0; i<32; i++) {
        x.pop_back();
        y.pop_back();
    }
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, pop_front0) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({});
    deque<int> y({});
    for (int i=0; i<64; i++) {
        x.push_back(i);
        y.push_back(i);
    }
    for (int i=0; i<32; i++) {
        x.pop_front();
        y.pop_front();
    }
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}


TYPED_TEST(DequeFixture, insert0) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({1, 2, 3, 4, 5});
    deque<int> y({1, 2, 3, 4, 5});
    x.insert(x.begin() + 2, 10);
    y.insert(y.begin() + 2, 10);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, insert1) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({});
    deque<int> y({});
    for (int i=0; i<64; i++) {
        x.push_back(i);
        y.push_back(i);
    }
    for (int i=0; i<32; i++) {
        ASSERT_EQ(*(x.insert(x.begin()+i, i)), i);
        y.insert(y.begin()+i, i);
    }
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, clear0) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({1,3,2,4,3,5});
    deque<int> y({1,3,2,4,3,5});
    x.clear();
    y.clear();
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}


TYPED_TEST(DequeFixture, clear1) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x;
    deque<int> y;
    x.clear();
    y.clear();
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, clear2) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x(0);
    deque<int> y(0);
    for (int i=0; i<64; i++) {
        x.push_back(i);
        y.push_back(i);
        x.push_front(i);
        y.push_front(i);
    }
    x.clear();
    y.clear();
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin(), y.end()));
}


TYPED_TEST(DequeFixture, back1) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({1,2,3,4,5});
    deque<int> y({1,2,3,4,5});

    ASSERT_EQ(x.back(), y.back());
    x.pop_back();
    y.pop_back();
    ASSERT_EQ(x.back(), y.back());
    x.pop_back();
    y.pop_back();
    ASSERT_EQ(x.back(), y.back());
}

TYPED_TEST(DequeFixture, front1) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({1,2,3,4,5});
    deque<int> y({1,2,3,4,5});

    ASSERT_EQ(x.front(), y.front());
    x.pop_front();
    y.pop_front();
    ASSERT_EQ(x.front(), y.front());
    x.pop_front();
    y.pop_front();
    ASSERT_EQ(x.front(), y.front());
}

TYPED_TEST(DequeFixture, equal1) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({1,2,3,4,5});
    deque_type y;
    for (int i=1; i<6; i++) {
        y.push_back(i);
    }
    deque_type z;
    for (int i=5; i>0; i--) {
        z.push_front(i);
    }

    ASSERT_TRUE(x==y);
    ASSERT_TRUE(y==x);
    ASSERT_TRUE(y==z);
    ASSERT_TRUE(z==y);
    ASSERT_TRUE(x==z);
    ASSERT_TRUE(z==x);
}


TYPED_TEST(DequeFixture, lessthan1) {
    using deque_type = typename TestFixture::deque_type;

    deque_type x({1,2,3,4,5});
    deque_type y({1,2,3,4,5,6});
    deque_type z({1,2,3,5,5});

    ASSERT_TRUE(x<y);
    ASSERT_TRUE(y<z);
    ASSERT_TRUE(x<z);
    ASSERT_TRUE(!(y<x));
    ASSERT_TRUE(!(z<y));
    ASSERT_TRUE(!(z<x));
}
